CKEditor Media Embed
********************

Description
***********

I've taken the CKEditor media embed plugin from Kent Safranski[1] and
wysiwygified it. I also slightly modified it not to use an iframe based dialog
(it was causing problems for me with Firefox at least).

Installation & Use
******************

1. Install the module the normal way[2].
2. Go to the WYSIWYG edit profile screen.
3. In the 'Buttons and plugins' fieldset enable 'Media Embed'.
4. You should now see a new button on the CKE toolbar.


[1] http://www.fluidbyte.net/embed-youtube-vimeo-etc-into-ckeditor
[2] http://drupal.org/documentation/install/modules-themes/modules-5-6

real author: Kent Safranski, http://www.fluidbyte.net/
Drupal author: AndyF
http://drupal.org/user/220112