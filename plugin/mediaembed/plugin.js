/*
* @file A simple textarea dialog to allow snippets of HTML to be inserted.
*/
( function() {
  CKEDITOR.plugins.add( 'MediaEmbed',
  {
    requires: [ 'iframedialog' ],
    init: function( editor )
    {
      var me = this;
      CKEDITOR.dialog.add( 'MediaEmbedDialog', function ( editor )
      {
        return {
          title : 'Embed Media Dialog',
          minWidth : 550,
          minHeight : 200,
          contents :
          [
            {
              id : 'mediaTab',
              label : 'Embed media code',
              title : 'Embed media code',
              elements :
              [
                {
                  id : 'embed',
                  type : 'textarea',
                  label : 'Paste embed code here',
                  cols: 200,
                  rows: 15
                }
              ]
            }
          ],
          onOk : function()
          {
            var content = this.getValueOf( 'mediaTab', 'embed' );
            final_html = 'MediaEmbedInsertData|---' + escape('<div class="media_embed">'+content+'</div>') + '---|MediaEmbedInsertData';
            editor.insertHtml(final_html);
            updated_editor_data = editor.getData();
            clean_editor_data = updated_editor_data.replace(final_html,'<div class="media_embed">'+content+'</div>');
            editor.setData(clean_editor_data);
          }
        };
      } );

      editor.addCommand( 'MediaEmbed', new CKEDITOR.dialogCommand( 'MediaEmbedDialog' ) );

      editor.ui.addButton( 'MediaEmbed',
      {
        label: 'Embed Media',
        command: 'MediaEmbed',
        icon: this.path + 'images/icon.gif'
      } );
    }
  } );
} )();